# CatalogoFilmes

Este projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) versão 8.1.2. Se trata de um projeto front-end onde você pode buscar por filmes e pegar algumas informações sobre eles. 

## Instalação

Você precisará ter o angular instalado em sua máquina. Com isto feito, ao clonar o repositório em sua máquina execute o comando `npm install` dentro do mesmo para que as dependências sejam baixadas.

## Servidor de desenvolvimento

Execute `ng serve` para subir o projeto. Abra o navegador em `http://localhost:4200/`. 

## Informações adicionais

Para usar este projeto é interessante que você obtenha uma chave própria após fazer seu registro em https://www.themoviedb.org/documentation/api.
A chave se encontra no arquivo Constants.ts. Mas para fins acadêmicos deixarei minha chave disponível, ou seja, ela já se encontra no arquivo.

## O seu protótipo é descartável ou evolucionário? Justifique.

Protótipo evolucionário. Porque caso mostrassemos ao cliente como está o projeto no momento, facilmente se poderia fazer melhorias a partir deles ou até implementar novos requisitos que surgissem afim de refinar este protótipo a cada entrega e se ter um melhor entendimento sobre os requisitos.

## Quais as características da linguagem escolhida fazem ela apropriada para o desenvolvimento de um protótipo?

Foi utilizado o angular, que basicamente se trata do Typescript, ou seja, além de trazer todas as qualidades que o javascript tem para uma prototipação, como leveza na hora de programar e menos preocupações com detalhes que outras linguagens trariam, ela é baseada em componentização, dessa forma se consegue facilmente pegar um componente já pronto e "encaixar" no seu código sem que você perca tempo ou se preocupe em codificar aquilo.

## Que dificuldades você teve no desenvolvimento do protótipo?

Não tive muita dificuldade. Talvez na parte da visualização, mas é algo que talvez nem precisasse se focar tanto por se tratar de um protótipo. Só que com o tempo que foi disponibilizado foi possível se preocupar um pouco mais.

## Quais as vantagens de ter um protótipo desenvolvido antes da implementação do programa efetivo?

Você entende muito melhor os requisitos necessários para a implementação do projeto, entende quais seriam possíveis gargalos que não tenham ficado claros na especificação dos requisitos, conhece melhor o código para executar determinadas tarefas, o que fará você perder bem menos tempo na hora de montar o projeto real, entre outras vantagens.
