import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NowPlayingBannerComponent } from './components/now-playing-banner/now-playing-banner.component';
import { HttpClientModule } from '@angular/common/http';
import { GenreListComponent } from './components/genre-list/genre-list.component';
import { GenreMovieListComponent } from './components/genre-movie-list/genre-movie-list.component';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { NoteFoundComponent } from './components/note-found/note-found.component';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenreMoviesComponent } from './components/genre-movies/genre-movies.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    AppComponent,
    NowPlayingBannerComponent,
    GenreListComponent,
    GenreMovieListComponent,
    MovieDetailsComponent,
    NoteFoundComponent,
    MovieSearchComponent,
    HeaderComponent,
    FooterComponent,
    GenreMoviesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxUsefulSwiperModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatInputModule,
    MatFormFieldModule,
    FontAwesomeModule,
    MatGridListModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
