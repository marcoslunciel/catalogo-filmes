export class Constants{

    public static api_key = "952100cef1e498c565274a1e36c1211d";
    public static language = "pt-BR";
    public static url_poster = "https://image.tmdb.org/t/p/original";
    public static popularityDesc = "&sort_by=popularity.desc";
    public static url_movie_discover = "https://api.themoviedb.org/3/discover/movie?api_key=";
    public static url_movie_get = "https://api.themoviedb.org/3/movie/"
    public static url_movie_search = "https://api.themoviedb.org/3/search/movie?api_key=";
    
}