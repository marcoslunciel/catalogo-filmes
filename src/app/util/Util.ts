import { OnInit } from '@angular/core';

export class Util implements OnInit{

    font: string;
    maximumSize = 26;
    minimumSize = 12;
    fontReset = 16;

    constructor(){
        this.fontConfiguration();
    };

    ngOnInit(){
        this.fontConfiguration();
    }

    public increaseFontListener(font: String): String
    {
        let fontSize: number = parseInt(font.replace(/\D/g, ""));
        if(fontSize < this.maximumSize)
        {
            fontSize++;
        }
        font = fontSize+"px";
        return font;
    }

    public decreaseFontListener(font: String): String
    {
        let fontSize: number = parseInt(font.replace(/\D/g, ""));
        if(fontSize > this.minimumSize)
        {
            fontSize--;
        }
        font = fontSize+"px";
        return font;
    }

    public fontResetListener(font: number): String
    {
        return font + 'px';
    }

    fontConfiguration(){
        document.addEventListener("font:increase", ()=>{
          this.font = this.increaseFontListener(this.font).toString();
        }, false);
    
        document.addEventListener("font:decrease", ()=>{
          this.font = this.decreaseFontListener(this.font).toString();
        }, false);

        document.addEventListener("font:reset", () =>{
            this.font = this.fontResetListener(this.fontReset).toString();
        });
    }
}