import { Component, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { faTextHeight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Catálogo de Filmes';
  faTextHeight = faTextHeight;
  public altoContraste =  false;

}
