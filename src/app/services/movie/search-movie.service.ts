import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchMovieResult } from 'src/app/model/search-movie-result';
import { Constants } from 'src/app/util/Constants';
import { Observable } from 'rxjs';
import { MovieDetail } from 'src/app/model/movie-detail';
import { MovieCredits } from 'src/app/model/movie-credits';


@Injectable({
  providedIn: 'root'
})
export class SearchMovieService {

  private url: string ;
  private urlMovieGet: string;
  private urlMovieSearch: string;

  constructor(private http: HttpClient) 
  { 
    this.url =  Constants.url_movie_discover;
    this.urlMovieGet = Constants.url_movie_get;
    this.urlMovieSearch = Constants.url_movie_search;
  }

  getNowPlaying(): Observable<SearchMovieResult>
  {
    let primaryDateLte  = new Date();
    let primaryDateGte = new Date();

    let dayLte: string = primaryDateLte.getDate().toString().length > 1 ? primaryDateLte.getDate().toString() : "0" + primaryDateLte.getDate();
    let monthLte: string = (primaryDateLte.getMonth() + 1).toString().length > 1 ? (primaryDateLte.getMonth() + 1).toString() : "0" + (primaryDateLte.getMonth() + 1);
   
    let dayGte = "01";
    primaryDateGte.setMonth(primaryDateGte.getMonth() - 1);
    let monthGte: string = (primaryDateGte.getMonth() + 1).toString().length > 1 ? (primaryDateGte.getMonth() + 1).toString() : "0" + (primaryDateGte.getMonth() + 1).toString();

    let primaryDateLteString: string = "" + primaryDateLte.getFullYear() + "-" + monthLte + "-" + dayLte;
    let primaryDateGteString: string = "" + primaryDateGte.getFullYear() + "-" + monthGte + "-" + dayGte ;

    let urlGet = this.url + Constants.api_key + "&language=" + Constants.language + "&primary_release_date.gte=" 
    + primaryDateGteString + "&primary_release_date.lte=" + primaryDateLteString + Constants.popularityDesc;
    return this.http.get<SearchMovieResult>(urlGet);
  }

  getMovieForGenre(genre: Number, popularity: boolean, pagina: Number): Observable<SearchMovieResult>
  {
    let urlGet = this.url + Constants.api_key + "&language="+ Constants.language + "&with_genres=" + genre;

    if(pagina != null)
    {
      urlGet = urlGet + "&page=" + pagina; 
    }

    if(popularity)
    {
      urlGet = urlGet + "&sort_by=popularity.desc";
    }

    return this.http.get<SearchMovieResult>(urlGet)
  } 

  getMovieByID(id): Observable<MovieDetail>
  {
    let urlGet = this.urlMovieGet + id + "?api_key=" + Constants.api_key + "&language=" + Constants.language;

    return this.http.get<MovieDetail>(urlGet);
  }

  getMovieByTitle(pesquisa: string, pagina: Number): Observable<SearchMovieResult>
  {
    // let numberOfPages = 1;

    let urlGet = this.urlMovieSearch + Constants.api_key + "&language=" + Constants.language + "&query=" + pesquisa;

    if(pagina != null)
    {
      urlGet = urlGet + "&page=" + pagina; 
    }

    return this.http.get<SearchMovieResult>(urlGet);
  }

  getMovieCastById(id): Observable<MovieCredits>
  {
    let urlGet = this.urlMovieGet + id + "/credits" +  "?api_key=" + Constants.api_key + "&language=" + Constants.language;

    return this.http.get<MovieCredits>(urlGet);
  }
}
