import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Genre } from 'src/app/model/genre';
import { Constants } from 'src/app/util/Constants';
import { SearchGenreResult } from 'src/app/model/search-genre-result';

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  private url: string = "https://api.themoviedb.org/3/genre/movie/list?api_key=";
  constructor(private http: HttpClient) { }

  getAll(): Observable<SearchGenreResult>{
    let urlGetAll = this.url + Constants.api_key + "&language=" + Constants.language;

    return this.http.get<SearchGenreResult>(urlGetAll);
  }

}
