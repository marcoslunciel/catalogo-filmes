import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { NoteFoundComponent } from './components/note-found/note-found.component';
import { NowPlayingBannerComponent } from './components/now-playing-banner/now-playing-banner.component';
import { AppComponent } from './app.component';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { GenreMoviesComponent } from './components/genre-movies/genre-movies.component';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: NowPlayingBannerComponent},
  {path: "movieDetail/:id", component: MovieDetailsComponent},
  {path: 'movieSearch/:search', component: MovieSearchComponent},
  {path: 'genreMovies/:id', component: GenreMoviesComponent},
  {path: "**", component: NoteFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
