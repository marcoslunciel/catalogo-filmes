import { Component, OnInit } from '@angular/core';
import { SearchMovieService } from 'src/app/services/movie/search-movie.service';
import { Movie } from 'src/app/model/movie';
import { SearchMovieResult } from 'src/app/model/search-movie-result';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Constants } from 'src/app/util/Constants';
import { EventEmitter } from '@angular/core';
import { Util } from 'src/app/util/Util';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.scss']
})
export class MovieSearchComponent extends Util implements OnInit {

  public movieList: Movie[] = [];
  private pesquisa: string;
  public pesquisaChanged: EventEmitter<string> = new EventEmitter();
  totalPages: Number;
  currentPage: number;
  public urlGetPoster: string;
  font = '16px';

  constructor(private searchMovieService: SearchMovieService,
              private route: ActivatedRoute,
              private router: Router) 
  {
    super();
    this.urlGetPoster = Constants.url_poster;
    this.pesquisaChanged.subscribe(data =>
      {
        this.searchChanged(data);
      });
    this.currentPage = 1;
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => 
    {
      let query = params.get('search');
      this.pesquisaSet(query);
    });
    this.searchMovieService.getMovieByTitle(this.pesquisa, this.currentPage).subscribe((data: SearchMovieResult) => 
    {
      this.movieList = data.results;
      this.totalPages = data.total_pages;
    });
  }

  searchChanged(event){  
    this.currentPage = 1;
    this.searchMovieService.getMovieByTitle(this.pesquisa, this.currentPage).subscribe((data: SearchMovieResult) => 
    {
      this.movieList = data.results;
      this.totalPages = data.total_pages;
    });
  }

  pesquisaSet(val: string)
  {
    this.pesquisa = val;
    this.pesquisaChanged.emit(this.pesquisa);
  }

  pesquisaChangedEmmiter(){
    return this.pesquisaChanged;
  }

  movieDetails(movie: Movie)
  {
    this.router.navigate(['movieDetail', movie.id]);
  }

  onScroll(){
    debugger;
    if(this.currentPage < this.totalPages)
    {
      this.currentPage++;
      this.searchMovieService.getMovieByTitle(this.pesquisa, this.currentPage).subscribe((data: SearchMovieResult) =>
      {
        this.movieList = this.movieList.concat(data.results);
      });
    }
  }
}
