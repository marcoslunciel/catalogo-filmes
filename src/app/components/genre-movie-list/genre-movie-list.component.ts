import { Component, OnInit, Input, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { Movie } from 'src/app/model/movie';
import { SearchMovieService } from 'src/app/services/movie/search-movie.service';
import { SearchMovieResult } from 'src/app/model/search-movie-result';
import { Router } from '@angular/router';
import { Constants } from 'src/app/util/Constants';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'ngx-useful-swiper';
import { Util } from 'src/app/util/Util';

@Component({
  selector: 'app-genre-movie-list',
  templateUrl: './genre-movie-list.component.html',
  styleUrls: ['./genre-movie-list.component.scss']
})
export class GenreMovieListComponent extends Util implements OnInit {

  public movieList: Movie[] = [];
  public sizeList: Number = 10;
  public urlGetPoster: string;
  public font = '16px';
  public maximumSize = 18;
  
  @Input('parentData') genreId: Number;
  @Input('indexData') indexData: number;

  @ViewChild('usefulSwiperGenreMovie', { static: true }) usefulSwiper: SwiperComponent;

  config: SwiperOptions = {
    autoHeight: true,
    allowTouchMove: true,
    setWrapperSize: true,
    slidesOffsetAfter: 90,
    breakpoints: {
      1024: {
        slidesPerView: 5
      },
      500: {
        slidesPerView: 3
      },
      400: {
        slidesPerView: 1
      },
      300: {
        slidesPerView: 1
      }
    },
    navigation:{
      nextEl: '.swipper-button-next',
      prevEl: '.swipper-button-prev'
    },
    loop: false
  }; 
  
  constructor(private searchMovieService: SearchMovieService,
              private router: Router) 
  {
    super();
    this.urlGetPoster = Constants.url_poster;
  }

  ngOnInit() {
    this.searchMovieService.getMovieForGenre(this.genreId, true, null).subscribe((data: SearchMovieResult) => 
    {
      for (let index = 0; index < this.sizeList; index++) {
        let movie = data.results[index];
        this.movieList[index] = movie;        
      }
    });
  }


  movieDetails(movie: Movie)
  {
    this.router.navigate(['movieDetail', movie.id]);
  }

  nextSlide() {
    this.usefulSwiper.swiper.slideNext();
  }
 
  previousSlide() {
    this.usefulSwiper.swiper.slidePrev();
  }

}
