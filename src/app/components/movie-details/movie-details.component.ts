import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { SearchMovieService } from 'src/app/services/movie/search-movie.service';
import { MovieDetail } from 'src/app/model/movie-detail';
import { MovieCredits } from 'src/app/model/movie-credits';
import { Cast } from 'src/app/model/cast';
import { Genre } from 'src/app/model/genre';
import { IfStmt } from '@angular/compiler';
import { Constants } from 'src/app/util/Constants';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { Util } from 'src/app/util/Util';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent extends Util implements OnInit {

  movieId: Number;
  movieDetail: MovieDetail;
  movieCredits: MovieCredits;
  directorName: String;
  mainActors: Cast[];
  mainActorsNumber = 3;
  genres: String = "";
  actors: String = "";
  urlGetPoster: String;
  faStar = faStar;
  font = '18px';

  constructor(private route: ActivatedRoute,
              private searchMovieService: SearchMovieService) 
  { 
    super();
    this.route.paramMap.subscribe((params: ParamMap) => 
    {
      let id = parseInt(params.get('id'));
      this.movieId = id;
    });

    this.urlGetPoster = Constants.url_poster;
  } 

  ngOnInit() {
    this.searchMovieService.getMovieByID(this.movieId).subscribe((data: MovieDetail) => 
    {
      this.movieDetail = data;
      for (let index = 0; index < this.movieDetail.genres.length; index++) {
        let genre: Genre = this.movieDetail.genres[index];
        if(index != this.movieDetail.genres.length - 1)
        {
          this.genres += genre.name + ", ";
        }else{
          this.genres += genre.name + "";
        }
      }
    });

    this.searchMovieService.getMovieCastById(this.movieId).subscribe((data: MovieCredits) => 
    {
      this.movieCredits = data;

      let foundedDirector = false;

      if(this.movieCredits != null)
      {
        for (let index = 0; index < this.movieCredits.crew.length && foundedDirector == false; index++) {
          let crew = this.movieCredits.crew[index];
          if(crew.job == "Director")
          {
            foundedDirector = true;
            this.directorName = crew.name;
          }
        }
        if(this.movieCredits.cast != null)
        {
          this.mainActors = [];
          let limitNumber = this.mainActorsNumber;
          if(this.movieCredits.cast.length < limitNumber)
          {
            limitNumber = this.movieCredits.cast.length
          }

          for (let index = 0; index < limitNumber; index++) {
            let actor = this.movieCredits.cast[index];
            this.mainActors[index] = actor;
            
            if(index != limitNumber - 1)
            {
              this.actors += actor.name + ", ";
            }else{
              this.actors += actor.name + "";
            }
          }
        }
      }

    });
  }

}
