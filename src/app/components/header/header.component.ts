import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faSearch, faAdjust } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  faSearch = faSearch;
  
  faAdjust = faAdjust;
  
  @Output() public altoContrasteEvent = new EventEmitter();

  public altoContraste = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onKey(event){
    if(event.key == 'Enter')
    {
      this.router.navigate(['movieSearch', event.srcElement.value]);
    }
  }

  increaseFont() {
    let event = new Event('font:increase');
    document.dispatchEvent(event);
  }

  decreaseFont() {
    let event = new Event('font:decrease');
    document.dispatchEvent(event);
  }

  resetFont() {
    let event = new Event('font:reset');
    document.dispatchEvent(event);
  }

  changeClass(){
    if(this.altoContraste == false)
    {
      this.altoContraste = true;
      this.altoContrasteEvent.emit('true');
    }else{
      this.altoContraste = false;
      this.altoContrasteEvent.emit('false');
    }
  }
}
