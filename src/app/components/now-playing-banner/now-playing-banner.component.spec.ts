import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NowPlayingBannerComponent } from './now-playing-banner.component';

describe('NowPlayingBannerComponent', () => {
  let component: NowPlayingBannerComponent;
  let fixture: ComponentFixture<NowPlayingBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NowPlayingBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NowPlayingBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
