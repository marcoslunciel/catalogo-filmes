import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchMovieService } from 'src/app/services/movie/search-movie.service';
import { SearchMovieResult } from 'src/app/model/search-movie-result';
import { Movie } from 'src/app/model/movie';
import { Constants } from 'src/app/util/Constants';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'ngx-useful-swiper';

@Component({
  selector: 'app-now-playing-banner',
  templateUrl: './now-playing-banner.component.html',
  styleUrls: ['./now-playing-banner.component.scss']
}) 
export class NowPlayingBannerComponent implements OnInit {

  public nowPlaying: string[] = [];
  public urlGetBanner: string;

  @ViewChild('usefulSwiper', { static: false }) usefulSwiper: SwiperComponent;
  
  configBanner: SwiperOptions = {
    pagination: {el:'.swiper-pagination', clickable: true},
    autoHeight: true,
    allowTouchMove: true,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
      stopOnLastSlide: false
    },
    breakpoints: {
      1024: {
        slidesPerView: 1
      },
      500: {
        slidesPerView: 1
      },
      400: {
        slidesPerView: 1
      },
      300: {
        slidesPerView: 1
      }
    },
    navigation:{
      nextEl: '.swipper-button-next',
      prevEl: '.swipper-button-prev'
    },
    loop: false
  }; 

  constructor(private searchMovieService: SearchMovieService) 
  { 
    this.urlGetBanner = Constants.url_poster;
  }

  ngOnInit() {
    try{
    this.searchMovieService.getNowPlaying().subscribe((data: SearchMovieResult) =>
      {
        let movies: Movie[] = data.results;
        for (let i = 0, j=0; i < 5; i++,j++) {
            let movie: Movie = movies[j];
            if(movie.backdrop_path != null && movie.backdrop_path != '')
            {
              this.nowPlaying[i] = movie.backdrop_path.toString();
            }else{
              i--;
            }
        }
      });

    }catch(error)
    {
      console.log(error);
    }
  }

  nextSlide() {
    this.usefulSwiper.swiper.slideNext();
  }
 
  previousSlide() {
    this.usefulSwiper.swiper.slidePrev();
  }
  
  slideToThis(index) {
    this.usefulSwiper.swiper.slideTo(index);
  }
}
