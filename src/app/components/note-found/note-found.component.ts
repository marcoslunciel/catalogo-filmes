import { Component, OnInit } from '@angular/core';
import { Util } from 'src/app/util/Util';

@Component({
  selector: 'app-note-found',
  templateUrl: './note-found.component.html',
  styleUrls: ['./note-found.component.scss']
})
export class NoteFoundComponent extends Util implements OnInit{

  font = '24px';
  fontReset = 24;
  
  constructor() {
    super();
  }

  ngOnInit() {
  }

}
