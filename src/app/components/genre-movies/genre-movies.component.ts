import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Genre } from 'src/app/model/genre';
import { Movie } from 'src/app/model/movie';
import { SearchMovieService } from 'src/app/services/movie/search-movie.service';
import { SearchMovieResult } from 'src/app/model/search-movie-result';
import { GenreService } from 'src/app/services/genre/genre.service';
import { SearchGenreResult } from 'src/app/model/search-genre-result';
import { Constants } from 'src/app/util/Constants';
import { Util } from 'src/app/util/Util';

@Component({
  selector: 'app-genre-movies',
  templateUrl: './genre-movies.component.html',
  styleUrls: ['./genre-movies.component.scss']
})
export class GenreMoviesComponent extends Util implements OnInit {

  genreId: Number;
  genre: Genre;
  movieList: Movie[] = [];
  urlGetPoster: string;
  totalPages: Number;
  currentPage: number;
  font = '16px';
  public maximumSize = 18;
  
  constructor(private route: ActivatedRoute,
              private service: SearchMovieService,
              private genreService: GenreService,
              private router: Router) 
  {
    super();
    this.route.paramMap.subscribe((param: ParamMap) => 
    {
      let id = parseInt(param.get('id'));
      this.genreId = id;
    });
    this.urlGetPoster = Constants.url_poster;
    this.currentPage = 1;
  }

  async ngOnInit(): Promise<void> {
    let data: SearchGenreResult = await this.genreService.getAll().toPromise();
    data.genres.forEach((genre: Genre) =>
    {
      if(genre.id == this.genreId)
      {
        this.genre = genre;
      }
    });

    this.service.getMovieForGenre(this.genre.id, true, this.currentPage).subscribe((data: SearchMovieResult) =>
    {
      this.movieList = data.results;
      this.totalPages = data.total_pages;
    });
  }

  onScroll(){
    if(this.currentPage < this.totalPages)
    {
      this.currentPage++;
      this.service.getMovieForGenre(this.genre.id, true, this.currentPage).subscribe((data: SearchMovieResult) =>
      {
        this.movieList = this.movieList.concat(data.results);
      });
    }
  }

  movieDetails(movie: Movie)
  {
    this.router.navigate(['movieDetail', movie.id]);
  }

}
