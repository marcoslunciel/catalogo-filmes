import { Component, OnInit } from '@angular/core';
import { Genre } from 'src/app/model/genre';
import { GenreService } from 'src/app/services/genre/genre.service';
import { SearchGenreResult } from 'src/app/model/search-genre-result';
import { Router } from '@angular/router';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.scss']
})
export class GenreListComponent implements OnInit {

  public genresMovies: Genre[] = [];
  
  constructor(private genreService: GenreService,
              private router: Router) { }
  
  ngOnInit() {
    try{
    this.genreService.getAll().subscribe((data: SearchGenreResult) => 
    {
      this.genresMovies = data.genres;
    });
    }catch(error)
    {
      console.log(error);
    }
  }

  moviesByGenre(genre)
  {
    this.router.navigate(['genreMovies', genre.id]);
  }

}
