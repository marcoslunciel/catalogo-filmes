import { Genre } from './genre';

export class SearchGenreResult{
    genres: Genre[];
}