export class Movie {
    
    popularity: Number;
    id: Number;
    video: Boolean;
    vote_count: Number;
    vote_average: Number;
    title: String;
    release_date: Date;
    original_language: String;
    original_title: String;
    genre_ids: Number[];
    backdrop_path: String; 
    adult: Boolean; 
    overview: String;
    poster_path: String;
    
}