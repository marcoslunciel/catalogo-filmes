export class Crew{

    credit_id: String;
    department: String;
    gender: Number;
    id: Number;
    job: String;
    name: String;
    profile_path: String;
    
}