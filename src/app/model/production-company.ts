export class ProductionCompany{
    
    id: Number;
    logo_path: String;
    name: String;
    origin_country: String;

}