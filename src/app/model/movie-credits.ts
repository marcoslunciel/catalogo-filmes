import { Cast } from './cast';
import { Crew } from './crew';

export class MovieCredits{
    id: Number;
    cast: Cast[];
    crew: Crew[];
}