import { Genre } from './genre';
import { ProductionCompany } from './production-company';
import { ProductionCountry } from './production-country';
import { SpokenLanguage } from './spoken-language';

export class MovieDetail{
    
    adult: Boolean;
    backdrop_path: String;
    belongs_to_collection: any;
    budget: Number;
    genres: Genre[];
    homepage: String;
    id: Number;
    imdb_id: Number;
    original_language: String;
    original_title: String;
    overview: String;
    popularity: Number;
    poster_path: String;
    production_companies: ProductionCompany[];
    production_countries: ProductionCountry[];
    release_date: Date;
    revenue: Number;
    runtime: Number;
    spoken_languages: SpokenLanguage[];
    status: String;
    tagline: String;
    title: String;
    video: Boolean;
    vote_average: Number;
    vote_count: Number;

}