export class Cast {

    cast_id: Number;
    character: String;
    credit_id: String;
    gender: Number;
    id: Number;
    name: String;
    order: Number;
    profile_path: String;
    
}