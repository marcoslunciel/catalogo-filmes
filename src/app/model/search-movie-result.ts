import { Movie } from './movie';

export class SearchMovieResult{
    
    page: Number;
    total_results: Number;
    total_pages: Number;
    results: any;
    
} 